﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Mapper
{
    /// <summary>
    /// 对象映射器
    /// </summary>
    /// <typeparam name="TIn"></typeparam>
    /// <typeparam name="TOut"></typeparam>
    public class ObjectMapperHelper<TIn, TOut>
    {
        private static Func<TIn, TOut> _MapperFunc_SourceMode = null;
        private static Func<TIn, TOut> _MapperFunc_TargetMode = null;

        private static Expression<Func<TIn, TOut>> _MapperExpression_SourceMode = null;
        private static Expression<Func<TIn, TOut>> _MapperExpression_TargetMode = null;

        static ObjectMapperHelper()
        {
            Type typeIn = typeof(TIn);
            Type typeOut = typeof(TOut);

            List<MemberBinding> memberBindings = new List<MemberBinding>();

            IEnumerable<MapperAttribute> mapperAttributes;
            MapperAttribute mapperAttribute;

            #region 生成目标模式委托

            PropertyInfo propInTemp;
            PropertyInfo[] propsOut = typeOut.GetProperties();
            ParameterExpression paramExpr = Expression.Parameter(typeIn, "o");
            foreach (var propOut in propsOut)
            {
                if (propOut.IsDefined(typeof(MapperAttribute), true))
                {
                    //获取映射特性
                    mapperAttributes = propOut.GetCustomAttributes<MapperAttribute>();
                    mapperAttribute = mapperAttributes.Where(attr => attr.IsMapping(typeIn)).OrderByDescending(o => o.MappingPriority).FirstOrDefault();
                    if (mapperAttribute != null)
                    {
                        propInTemp = mapperAttribute.GetMappingProperty(typeIn);
                        if (propInTemp == null || !mapperAttribute.IsMapping(propInTemp))
                            continue;
                        memberBindings.Add(Expression.Bind(propOut, Expression.Property(paramExpr, propInTemp)));
                    }
                }

                if ((propInTemp = typeIn.GetProperty(propOut.Name)) != null)
                {
                    memberBindings.Add(Expression.Bind(propOut, Expression.Property(paramExpr, propInTemp)));
                }
            }

            _MapperExpression_TargetMode = Expression.Lambda<Func<TIn, TOut>>(
                   Expression.MemberInit(
                       Expression.New(typeOut), memberBindings), paramExpr);

            _MapperFunc_TargetMode = _MapperExpression_TargetMode.Compile();

            #endregion

            #region 生成源模式委托

            /**
             * 遍历数据源的属性
             * 获取数据源属性上的特性标识
             * 
             */

            PropertyInfo propOutTemp;
            memberBindings.Clear();
            PropertyInfo[] propsIn = typeIn.GetProperties();
            foreach (var propIn in propsIn)
            {
                if (propIn.IsDefined(typeof(MapperAttribute), true))
                {
                    //获取映射特性
                    mapperAttributes = propIn.GetCustomAttributes<MapperAttribute>();
                    mapperAttribute = mapperAttributes.Where(attr => attr.IsMapping(typeIn)).OrderByDescending(o => o.MappingPriority).FirstOrDefault();
                    if (mapperAttribute != null)
                    {
                        propOutTemp = mapperAttribute.GetMappingProperty(typeIn);
                        if (propOutTemp == null || !mapperAttribute.IsMapping(propOutTemp))
                            continue;
                        memberBindings.Add(Expression.Bind(propOutTemp, Expression.Property(paramExpr, propIn)));
                    }
                }

                if ((propOutTemp = typeOut.GetProperty(propIn.Name)) != null)
                {
                    memberBindings.Add(Expression.Bind(propOutTemp, Expression.Property(paramExpr, propIn)));
                }
            }

            _MapperExpression_SourceMode = Expression.Lambda<Func<TIn, TOut>>(
                    Expression.MemberInit(
                        Expression.New(typeOut), memberBindings), paramExpr);

            _MapperFunc_SourceMode = _MapperExpression_SourceMode.Compile();

            #endregion
        }


        /// <summary>
        /// 转换对象
        /// </summary>
        /// <param name="tIn">需要转换的数据源</param>
        /// <param name="mapperMode">映射转换模式</param>
        /// <returns></returns>
        public static TOut Transform(TIn tIn, MapperMode mapperMode = MapperMode.Target)
        {
            if (mapperMode == MapperMode.Source)
                return _MapperFunc_SourceMode(tIn);
            else if (mapperMode == MapperMode.Target)
                return _MapperFunc_TargetMode(tIn);
            return default(TOut);
        }
    }
}
