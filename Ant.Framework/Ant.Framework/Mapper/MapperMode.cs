﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Mapper
{
    /// <summary>
    /// 映射转换模式
    /// </summary>
    [Flags]
    public enum MapperMode
    {
        /// <summary>
        /// 目标模式,转换时以目标类的<see cref="MapperAttribute">Mapper</see>特性为准
        /// </summary>
        Target = 1,
        /// <summary>
        /// 源模式,转换时以源类的<see cref="MapperAttribute">Mapper</see>特性为准
        /// </summary>
        Source = 2
    }
}
