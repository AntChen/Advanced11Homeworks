﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Mapper
{
    /// <summary>
    /// 映射属性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class MapperAttribute : Attribute
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="propertyName">属性名称</param>
        /// <param name="classType">类型 设为Null 即不指定，但优先级会低于设定了值的</param>
        public MapperAttribute(string propertyName, Type classType = null)
        {
            this.PropertyName = propertyName;
            this.ClassType = classType;
        }

        /// <summary>
        /// 映射的目标属性
        /// </summary>
        public string PropertyName { get; private set; }

        /// <summary>
        /// 映射的源属性
        /// </summary>
        public Type ClassType { get; private set; }

        /// <summary>
        /// 获取相对于其他同样的特性的优先级
        /// </summary>
        /// <returns></returns>
        public int MappingPriority
        {
            get { return ClassType == null ? 0 : 1; }
        }

        /// <summary>
        /// 是否可以映射为某个类型
        /// </summary>
        /// <param name="propIn">输入属性</param>
        /// <param name="propThis">当前输出的属性</param>
        /// <returns></returns>
        public virtual bool IsMapping(Type type)
        {
            return ClassType == null ? true : ClassType.Equals(type);
        }

        /// <summary>
        /// 是否可以映射
        /// </summary>
        /// <param name="propIn">输入属性</param>
        /// <param name="propThis">当前输出的属性</param>
        /// <returns></returns>
        public virtual bool IsMapping(PropertyInfo propIn)
        {
            return propIn.Name.Equals(PropertyName);
        }

        /// <summary>
        /// 通过 输入类型 与 当前输出属性 获取 输入属性
        /// </summary>
        /// <param name="typeIn">输入类型</param>
        /// <returns></returns>
        public virtual PropertyInfo GetMappingProperty(Type typeIn)
        {
            return typeIn.GetProperty(PropertyName);
        }
    }

}
