﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 数据库实体映射帮助类
    /// </summary>
    public static class MappigHelper
    {

        #region 表名获取

        /// <summary>
        /// 获取数据库表名
        /// <para> 使用 <see cref="TableAttribute"/> 特性来标记数据库表名信息</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string GetTableName<T>()
        {
            return GetTableName(typeof(T));
        }

        #region 拓展方法
        /// <summary>
        /// 获取数据库表名
        /// <para> 使用 <see cref="TableAttribute"/> 特性来标记数据库表名信息</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetTableName(this Type type)
        {
            return type.IsDefined(typeof(TableAttribute), true) ? ((TableAttribute)type.GetCustomAttribute(typeof(TableAttribute), true)).TableName : type.Name;
        }
        #endregion

        #endregion

        #region 列名获取

        #region 拓展方法
        /// <summary>
        /// 获取该属性对应的数据库字段名
        /// </summary>
        /// <param name="propInfo">属性信息</param>
        /// <returns>字段名或标记了 <see cref="ColumnAttribute"/> 的列名</returns>
        public static string GetColumnName(this PropertyInfo propInfo)
        {
            return propInfo.IsDefined(typeof(ColumnAttribute), true) ?
                         ((ColumnAttribute)propInfo.GetCustomAttribute(typeof(ColumnAttribute), true)).ColumnName
                         : propInfo.Name;
        }

        /// <summary>
        /// 是否为数据库表主键
        /// </summary>
        /// <param name="propInfo">属性信息</param>
        /// <returns>字段名或标记了 <see cref="ColumnAttribute"/> 的主键信息</returns>
        public static bool IsPrimaryKey(this PropertyInfo propInfo)
        {
            return propInfo.IsDefined(typeof(ColumnAttribute), true) ?
                         ((ColumnAttribute)propInfo.GetCustomAttribute(typeof(ColumnAttribute), true)).IsPrimaryKey
                         : false;
        }
        #endregion

        /// <summary>
        /// 获取含有数据库表主键的属性信息
        /// <para> 使用 <see cref="ColumnAttribute"/> 特性来标记主键信息</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static PropertyInfo GetPrimaryKeyProperty<T>()
        {
            return GetPrimaryKeyProperty(typeof(T));
        }
        /// <summary>
        /// 获取含有数据库表主键的属性信息
        /// <para> 使用 <see cref="ColumnAttribute"/> 特性来标记主键信息</para>
        /// </summary>
        /// <param name="type">需要获取的类型</param>
        /// <returns></returns>
        public static PropertyInfo GetPrimaryKeyProperty(Type type)
        {
            return type.GetProperties().Where(prop =>
            prop.IsPrimaryKey())
            .FirstOrDefault();
        }
        /// <summary>
        /// 是否含有数据库表主键 
        /// <para> 使用 <see cref="ColumnAttribute"/> 特性来标记主键信息</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool HasPrimaryKey<T>()
        {
            return HasPrimaryKey(typeof(T));
        }
        /// <summary>
        /// 是否含有数据库表主键 
        /// <para> 使用 <see cref="ColumnAttribute"/> 特性来标记主键信息</para>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool HasPrimaryKey(Type type)
        {
            return type.GetProperties().Where(prop =>
                prop.IsPrimaryKey())
                .Count() > 0;
        }
        #endregion

    }
}
