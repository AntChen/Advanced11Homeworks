﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 数据库列特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        /// <summary>
        /// 数据库列特性 构造方法
        /// </summary>
        /// <param name="columnName">数据库表列名</param>
        /// <param name="isPrimaryKey">是否是主键</param>
        /// <param name="isAutoGrowth">是否是自动增长</param>
        public ColumnAttribute(string columnName, bool isPrimaryKey = false, bool isAutoGrowth = false)
        {
            this.ColumnName = columnName;
            this.IsPrimaryKey = isPrimaryKey;
            this.IsAutoGrowth = isAutoGrowth;
        }
        /// <summary>
        /// 数据库表列名
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 是否是主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// 是否是自动增长
        /// </summary>
        public bool IsAutoGrowth { get; set; }

    }
}
