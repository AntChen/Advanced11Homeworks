﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 正则验证
    /// </summary>
    public class RegexAttribute : AbstractValidateAttribute
    {
        /// <summary>
        /// 正则字符串
        /// </summary>
        public string RegexStr { get; set; }

        /// <summary>
        /// 正则验证  构造方法
        /// </summary>
        /// <param name="regexStr"></param>
        public RegexAttribute(string regexStr)
        {
            this.RegexStr = regexStr;
        }

        public override bool Validate(object value)
        {
            return value != null && !string.IsNullOrWhiteSpace(value.ToString()) && System.Text.RegularExpressions.Regex.IsMatch(value.ToString(), this.RegexStr);
        }
    }
}
