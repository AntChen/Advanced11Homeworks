﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{

    /// <summary>
    /// 邮箱验证
    /// </summary>
    public class EmailAttribute : RegexAttribute
    {
        /// <summary>
        /// 邮箱验证  构造方法
        /// </summary>
        public EmailAttribute()
            //: base(@"^[a-z]([a-z0-9]*[-_]?[a-z0-9]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+[\.][a-z]{2,3}([\.][a-z]{2})?$")
            : base(@"[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?")
        {

        }
    }
}
