﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 手机号码验证
    /// </summary>
    public class MobileAttribute : RegexAttribute
    {
        /// <summary>
        /// 手机号码验证  构造方法
        /// </summary>
        public MobileAttribute()
            : base(@"^((\(\d{2,3}\))|(\d{3}\-))?1{3,5-9}\d{9}$")
        {

        }
    }
}
