﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 验证特性类 基类
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true)]
    public abstract class AbstractValidateAttribute : Attribute
    {
        /// <summary>
        /// 验证失败的提示消息
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// 验证是否符合
        /// </summary>
        /// <param name="value">被验证的数据</param>
        /// <returns></returns>
        public abstract bool Validate(object value);
    }
}
