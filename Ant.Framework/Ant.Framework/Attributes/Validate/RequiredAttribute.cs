﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{

    /// <summary>
    /// 非空验证
    /// </summary>
    public class RequiredAttribute : AbstractValidateAttribute
    {
        /// <summary>
        /// 是否必需
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// 非空验证 构造方法
        /// </summary>
        /// <param name="isRequired"></param>
        public RequiredAttribute(bool isRequired = true)
        {
            this.IsRequired = isRequired;
        }

        public override bool Validate(object value)
        {
            return !IsRequired || value != null && !string.IsNullOrWhiteSpace(value.ToString());
        }
    }
}
