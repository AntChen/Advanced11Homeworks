﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 范围验证
    /// </summary>
    public class RangeAttribute : AbstractValidateAttribute
    {
        /// <summary>
        /// 最小最
        /// </summary>
        public double Min { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public double Max { get; set; }

        /// <summary>
        /// 是否包含最大值 默认不包含
        /// </summary>
        public bool ContainMax { get; set; } = false;

        /// <summary>
        /// 是否包含最小值 默认不包含
        /// </summary>
        public bool ContainMin { get; set; } = false;

        /// <summary>
        /// 范围验证 构造方法
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        public RangeAttribute(int min, int max)
        {
            this.Max = max;
            this.Min = min;
        }
        /// <summary>
        /// 范围验证 构造方法
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        public RangeAttribute(long min, long max)
        {
            this.Max = max;
            this.Min = min;
        }
        /// <summary>
        /// 范围验证 构造方法
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        public RangeAttribute(double min, double max)
        {
            this.Max = max;
            this.Min = min;
        }

        public override bool Validate(object value)
        {
            return value != null && !string.IsNullOrWhiteSpace(value.ToString())
                && double.TryParse(value.ToString(), out double val)
                && (val > this.Min || (ContainMin & val == this.Min))
                && (val < this.Max || (ContainMax & val == this.Max));
        }
    }
}
