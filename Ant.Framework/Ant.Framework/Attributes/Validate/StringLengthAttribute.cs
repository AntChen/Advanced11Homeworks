﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 字符串长度验证
    /// </summary>
    public class StringLengthAttribute : AbstractValidateAttribute
    {
        public long Min { get; set; }
        public long Max { get; set; }

        /// <summary>
        /// 字符串长度验证 构造方法
        /// </summary>
        /// <param name="min">最小长度</param>
        /// <param name="max">最大长度</param>
        public StringLengthAttribute(long min, long max = long.MaxValue)
        {
            this.Max = max;
            this.Min = min;
        }
        /// <summary>
        /// 字符串长度验证 构造方法
        /// </summary>
        /// <param name="min">最小长度</param>
        /// <param name="max">最大长度</param>
        public StringLengthAttribute(int min, int max = int.MaxValue)
        {
            this.Max = max;
            this.Min = min;
        }

        public override bool Validate(object value)
        {
            int length = value.ToString().Length;
            return value != null && !string.IsNullOrWhiteSpace(value.ToString())
                && length >= this.Min && length <= this.Max;
        }
    }
}
