﻿using Ant.Framework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Framework.Attributes
{
    /// <summary>
    /// 验证帮助类
    /// </summary>
    public static class ValidateHelper
    {

        #region 数据验证

        /// <summary>
        /// 数据验证  
        /// <para>请在类中使用基于特性<see cref="AbstractValidateAttribute"/>派生出来的特性类来标记字段 </para>
        /// <para>目前提供 <see cref="RequiredAttribute"/>、 <see cref="RangeAttribute"/>、 <see cref="StringLengthAttribute"/> 、 <see cref="RegexAttribute"/> </para>
        /// </summary>
        /// <param name="oObject">需要验证的实体</param>
        /// <remarks>请在类中使用基于特性[AbstractValidateAttribute]派生出来的类</remarks>
        /// <returns><code>true</code> 验证通过；<code>false</code> 验证不通过</returns>
        public static bool Validate(object oObject)
        {
            Type type = oObject.GetType();
            foreach (var prop in type.GetProperties())
            {
                if (prop.IsDefined(typeof(AbstractValidateAttribute), true))
                {
                    object[] attributeArray = prop.GetCustomAttributes(typeof(AbstractValidateAttribute), true);
                    foreach (AbstractValidateAttribute attribute in attributeArray)
                    {
                        if (!attribute.Validate(prop.GetValue(oObject)))
                        {
                            return false;//表示终止
                        }
                    }
                }
            }
            return true;
        }
        /// <summary>
        /// 数据验证  此方法会检查所有标记有<see cref="AbstractValidateAttribute"/>派生特性的字段
        /// <para>请在类中使用基于特性<see cref="AbstractValidateAttribute"/>派生出来的特性类来标记字段 </para>
        /// <para>目前提供 <see cref="RequiredAttribute"/>、 <see cref="RangeAttribute"/>、 <see cref="StringLengthAttribute"/> 、 <see cref="RegexAttribute"/> </para>
        /// </summary>
        /// <param name="oObject">需要验证的实体</param>
        /// <param name="errorMsgs">错误消息列表</param>
        /// <remarks>请在类中使用基于特性[AbstractValidateAttribute]派生出来的类</remarks>
        /// <returns><code>true</code> 验证通过；<code>false</code> 验证不通过</returns>
        public static bool Validate(object oObject, out List<string> errorMsgs)
        {
            Type type = oObject.GetType();
            errorMsgs = new List<string>();
            foreach (var prop in type.GetProperties())
            {
                if (prop.IsDefined(typeof(AbstractValidateAttribute), true))
                {
                    object[] attributeArray = prop.GetCustomAttributes(typeof(AbstractValidateAttribute), true);
                    foreach (AbstractValidateAttribute attribute in attributeArray)
                    {
                        if (!attribute.Validate(prop.GetValue(oObject)))
                        {
                            errorMsgs.Add(attribute.ErrorMessage);
                        }
                    }
                }
            }
            return errorMsgs.Count == 0;
        }

        /// <summary>
        /// 数据验证  
        /// <para>请在类 T 中使用基于特性<see cref="AbstractValidateAttribute"/>派生出来的特性类来标记字段 </para>
        /// <para>目前提供 <see cref="RequiredAttribute"/>、 <see cref="RangeAttribute"/>、 <see cref="StringLengthAttribute"/> 、 <see cref="RegexAttribute"/> </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t">数据实体</param>
        /// <returns><code>true</code> 验证通过；<code>false</code> 验证不通过</returns>
        public static bool Validate<T>(this T t) where T : BaseModel
        {
            Type type = t.GetType();
            foreach (var prop in type.GetProperties())
            {
                if (prop.IsDefined(typeof(AbstractValidateAttribute), true))
                {
                    object[] attributeArray = prop.GetCustomAttributes(typeof(AbstractValidateAttribute), true);
                    foreach (AbstractValidateAttribute attribute in attributeArray)
                    {
                        if (!attribute.Validate(prop.GetValue(t)))
                        {
                            return false;//表示终止
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 数据验证  此方法会检查所有标记有<see cref="AbstractValidateAttribute"/>派生特性的字段
        /// <para>请在类 T 中使用基于特性<see cref="AbstractValidateAttribute"/>派生出来的特性类来标记字段 </para>
        /// <para>目前提供 <see cref="RequiredAttribute"/>、 <see cref="RangeAttribute"/>、 <see cref="StringLengthAttribute"/> 、 <see cref="RegexAttribute"/> </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="errorMsgs">错误消息列表</param>
        /// <returns><code>true</code> 验证通过；<code>false</code> 验证不通过</returns>
        public static bool Validate<T>(this T t, out List<string> errorMsgs) where T : BaseModel
        {
            Type type = t.GetType();
            errorMsgs = new List<string>();
            foreach (var prop in type.GetProperties())
            {
                if (prop.IsDefined(typeof(AbstractValidateAttribute), true))
                {
                    object[] attributeArray = prop.GetCustomAttributes(typeof(AbstractValidateAttribute), true);
                    foreach (AbstractValidateAttribute attribute in attributeArray)
                    {
                        if (!attribute.Validate(prop.GetValue(t)))
                        {
                            //return false;//表示终止
                            errorMsgs.Add(attribute.ErrorMessage);
                        }
                    }
                }
            }
            return errorMsgs.Count == 0;
        }
        #endregion
    }
}
