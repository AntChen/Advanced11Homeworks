﻿using Ruanmou.Framework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ruanmou.Framework.AttributeExtend;

namespace Ruanmou.Libraries.DAL
{
    public class TSqlHelper<T> where T : BaseModel
    {
        static TSqlHelper()
        {
            Type type = typeof(T);
            string columnString = string.Join(",", type.GetProperties().Select(p => $"[{p.GetColumnName()}]"));
            string columnInsertString = string.Join(",", type.GetProperties().Where(p => !p.Name.Equals("Id")).Select(p => $"@{p.GetColumnName()}"));
            string columnUpdateString = string.Join(",", type.GetProperties().Where(p => !p.Name.Equals("Id")).Select(p => $"{p.GetColumnName()}=@{p.GetColumnName()}"));

            FindSql = $"SELECT {columnString} FROM [{type.Name}] WHERE Id=";
            FindAllSql = $"SELECT {columnString} FROM [{type.Name}];";
            DeleteByIdSql = $"DELETE FROM [{type.Name}] WHERE Id=";

            UpdateAllFieldByIdSql = $"UPDATE FROM [{type.Name}] SET {columnUpdateString} WHERE Id=";
            InsertWithoutIdSql = $"INSERT INTO [{type.Name}] VALUES({columnInsertString})";
        }

        public static string FindSql = null;
        public static string FindAllSql = null;
        public static string DeleteByIdSql = null;
        public static string UpdateAllFieldByIdSql = null;
        public static string InsertWithoutIdSql = null;
        //delete  update  insert 
    }
}
