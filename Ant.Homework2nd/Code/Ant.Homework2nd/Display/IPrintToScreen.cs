﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework2nd.Display
{
    public interface IPrintToScreen
    {
        void Print<T>(params T[] models);
        void Print<T>(List<T> models);
    }
}
