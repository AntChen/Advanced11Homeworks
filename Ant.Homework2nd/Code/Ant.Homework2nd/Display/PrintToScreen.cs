﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Ant.Homework2nd.Display
{
    public class PrintToScreen : IPrintToScreen
    {
        public void Print<T>(params T[] models)
        {
            Print(models.ToList());
        }
        public void Print<T>(List<T> models)
        {
            if (models != null && models.Count > 0)
            {
                var properties = typeof(T).GetProperties();
                var fields = typeof(T).GetFields();
                foreach (var model in models)
                {
                    Console.Write("字段：");
                    foreach (var field in fields)
                    {
                        Console.Write($">> {field.Name} : {field.GetValue(model)}    ");
                    }
                    Console.WriteLine();
                    Console.Write("属性：");
                    foreach (var prop in properties)
                    {
                        Console.Write($"> {prop.Name} : {prop.GetValue(model)}    ");
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine();
            }
        }
    }

}
