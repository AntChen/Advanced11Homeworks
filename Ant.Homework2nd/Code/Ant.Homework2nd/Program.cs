﻿using Ant.Homework2nd.Display;
using Ant.Homework2nd.Performers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework2nd
{
    class Program
    {
        static void Main(string[] args)
        {
            IPrintToScreen printToScreen = new PrintToScreen();

            var Southern = Show<SouthernPerformer>(printToScreen, per =>
             {
                 per.OnFire += (p) => Console.WriteLine("夫起大呼，妇亦起大呼。两儿齐哭。俄而百千人大呼，百千儿哭，百千犬吠。");
             });

            Show<NorthernPerformer>(printToScreen);

            Show<WesternPerformer>(printToScreen, per => {
                per.Table = "方桌";
                per.OnFire += p => Console.WriteLine("老鼠跑、猫跑、狗跑");
            });

            Show<EasternPerformer>(printToScreen);

            Console.ReadKey();
        }

        static T Show<T>(IPrintToScreen printToScreen) where T : Performer
        {
            Type type = typeof(T);
            T performer = (T)Activator.CreateInstance(type);
            printToScreen.Print(performer);
            performer.Show();
            return performer;
        }

        static T Show<T>(IPrintToScreen printToScreen, Action<T> action) where T : Performer
        {
            Type type = typeof(T);
            T performer = (T)Activator.CreateInstance(type);
            action?.Invoke(performer);
            printToScreen.Print(performer);
            performer.UpdateFireTemperature(900);
            performer.Show();
            return performer;
        }
    }
}
