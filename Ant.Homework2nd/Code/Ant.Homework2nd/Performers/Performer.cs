﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework2nd.Performers
{
    public delegate void PerformerEventHandler(Performer performer);
    /// <summary>
    /// 口技表演者
    /// </summary>
    public abstract class Performer : IChargeForPerformance
    {
        public virtual string People { get; set; } = "人";
        public virtual string Table { get; set; } = "桌";
        public virtual string Chair { get; set; } = "椅";
        public virtual string Fan { get; set; } = "扇";
        public virtual string Ruler { get; set; } = "抚尺";

        //火起温度
        protected virtual int FireTemperature { get; } = 400;

        protected virtual ConsoleColor ShowColor { get; } = ConsoleColor.White;

        public event PerformerEventHandler OnFire;
        public event PerformerEventHandler OnStart;
        public event PerformerEventHandler OnEnding;
        public event PerformerEventHandler OnHighTide;

        public void Start()
        {
            Console.WriteLine("表演开始了");
        }

        public abstract void DogsBark();
        public abstract void PeopleVoice();
        public abstract void SoundOfWind();

        protected abstract void UniqueTechniqueTime();

        /// <summary>
        /// 说开场白
        /// </summary>
        public virtual void SayPrologue()
        {
            Console.WriteLine("表演开场白");
        }
        /// <summary>
        /// 说结束语
        /// </summary>
        public virtual void SayClosingWords()
        {
            Console.WriteLine("表演结束语");
        }
        /// <summary>
        /// 表演绝活
        /// </summary>
        public virtual void ShowUniqueTechnique()
        {
            Console.WriteLine("绝活马上开始了。。。");
            UniqueTechniqueTime();
            Console.WriteLine("绝活表演结束，大家鼓掌！！");
        }

        public void Show()
        {
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ShowColor;

            Start();
            SayPrologue();

            OnStart?.Invoke(this);

            DogsBark();
            PeopleVoice();
            SoundOfWind();

            ShowUniqueTechnique();

            OnHighTide?.Invoke(this);

            SayClosingWords();
            FeeCharging();

            OnEnding?.Invoke(this);

            Console.ForegroundColor = color;
        }

        public void UpdateFireTemperature(int temperature)
        {
            if (temperature >= this.FireTemperature)
            {
                OnFire?.Invoke(this);
            }
        }

        public abstract void FeeCharging();
    }
}
