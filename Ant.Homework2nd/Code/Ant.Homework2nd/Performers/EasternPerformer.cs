﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework2nd.Performers
{
    /// <summary>
    /// 东派口技表演者
    /// </summary>
    public class EasternPerformer : Performer
    {
        protected override ConsoleColor ShowColor => ConsoleColor.Cyan;
        public override void DogsBark()
        {
            Console.WriteLine("东派模仿狗叫");
        }

        public override void FeeCharging()
        {
            Console.WriteLine("东派表演结束，收费");
        }

        public override void PeopleVoice()
        {
            Console.WriteLine("东派模仿人哭声");
        }

        public override void SoundOfWind()
        {
            Console.WriteLine("东派模仿风声");
        }

        protected override void UniqueTechniqueTime()
        {
            Console.WriteLine("东派绝活表演中");
        }
    }
}
