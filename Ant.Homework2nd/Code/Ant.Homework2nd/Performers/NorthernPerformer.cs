﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework2nd.Performers
{
    /// <summary>
    /// 北派口技表演者
    /// </summary>
    public class NorthernPerformer : Performer
    {
        protected override int FireTemperature => 1000;
        protected override ConsoleColor ShowColor => ConsoleColor.Yellow;

        public override void DogsBark()
        {
            Console.WriteLine("北派模仿狗叫");
        }

        public override void FeeCharging()
        {
            Console.WriteLine("北派表演结束，收费");
        }

        public override void PeopleVoice()
        {
            Console.WriteLine("北派模仿人哭声");
        }

        public override void SoundOfWind()
        {
            Console.WriteLine("北派模仿风声");
        }

        protected override void UniqueTechniqueTime()
        {
            Console.WriteLine("北派绝活表演中");
        }
        public override void SayClosingWords()
        {
            Console.WriteLine("北派表演结束语：谢谢大家捧场观看");
        }
    }
}
