﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework2nd.Performers
{
    /// <summary>
    /// 西派口技表演者
    /// </summary>
    public class WesternPerformer : Performer
    {
        protected override ConsoleColor ShowColor => ConsoleColor.Red;
        public override void DogsBark()
        {
            Console.WriteLine("西派模仿狗叫：汪汪汪汪汪汪");
        }

        public override void FeeCharging()
        {
            Console.WriteLine("西派表演结束，收费100");
        }

        public override void PeopleVoice()
        {
            Console.WriteLine("西派模仿哭声：呜呜呜呜呜呜呜呜呜呜呜");
        }

        public override void SoundOfWind()
        {
            Console.WriteLine("西派模仿风声：呼呼呼呼");
        }

        protected override void UniqueTechniqueTime()
        {
            Console.WriteLine("西派绝活表演中");
        }
        public override void SayPrologue()
        {
            Console.WriteLine("西派开场白：锣鼓震天。。。");
        }
        public override void SayClosingWords()
        {
            Console.WriteLine("西派表演结束语：谢谢大家，明日继续。");
        }
    }
}
