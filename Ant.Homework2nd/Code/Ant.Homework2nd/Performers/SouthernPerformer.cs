﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework2nd.Performers
{
    /// <summary>
    /// 南派口技表演者
    /// </summary>
    public class SouthernPerformer : Performer
    {
        protected override ConsoleColor ShowColor => ConsoleColor.Green;
        protected override int FireTemperature => 800;

        public override void DogsBark()
        {
            Console.WriteLine("南派模仿狗叫：旺旺");
        }

        public override void FeeCharging()
        {
            Console.WriteLine("南派表演结束，收费100");
        }

        public override void PeopleVoice()
        {
            Console.WriteLine("南派模仿人哭声：wuwuwuwuwuwuw");
        }

        public override void SoundOfWind()
        {
            Console.WriteLine("南派模仿风声：huhuhuhuhuhu");
        }

        protected override void UniqueTechniqueTime()
        {
            Console.WriteLine("南派绝活表演中~~~~~ 大变活人");
        }

        public override void SayPrologue()
        {
            Console.WriteLine("南派开场白：咚咚隆冬强");
        }
    }
}
