﻿using Ant.DB.ModelMapping.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.ModelMapping
{
    public static class MappingTool
    {
        #region 表名获取

        /// <summary>
        /// 获取数据库表名
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string GetTableName<T>()
        {
            return GetTableName(typeof(T));
        }
        /// <summary>
        /// 获取数据库表名
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetTableName(Type type)
        {
            return type.IsDefined(typeof(TableAttribute), true) ? ((TableAttribute)type.GetCustomAttribute(typeof(TableAttribute), true)).GetDBTableName() : type.Name;
        }

        #endregion

        #region 列名获取

        #region 拓展方法
        /// <summary>
        /// 获取该属性对应的数据库字段名
        /// </summary>
        /// <param name="propInfo"></param>
        /// <returns></returns>
        public static string GetColumnName(this PropertyInfo propInfo)
        {
            return propInfo.IsDefined(typeof(ColumnAttribute), true) ?
                         ((ColumnAttribute)propInfo.GetCustomAttribute(typeof(ColumnAttribute), true)).GetDBColumnName()
                         : propInfo.Name;
        }
        /// <summary>
        /// 是否是数字类型
        /// </summary>
        /// <param name="propInfo"></param>
        /// <returns></returns>
        public static bool IsNumberType(this PropertyInfo propInfo)
        {
            return propInfo.PropertyType.Equals(typeof(int))
                 || propInfo.PropertyType.Equals(typeof(double))
                 || propInfo.PropertyType.Equals(typeof(decimal))
                 || propInfo.PropertyType.Equals(typeof(long));
        }
        /// <summary>
        /// 是否为数据库表主键
        /// </summary>
        /// <param name="propInfo"></param>
        /// <returns></returns>
        public static bool IsPrimaryKey(this PropertyInfo propInfo)
        {
            return propInfo.IsDefined(typeof(ColumnAttribute), true) ?
                         ((ColumnAttribute)propInfo.GetCustomAttribute(typeof(ColumnAttribute), true)).IsPrimaryKey
                         : false;
        }
        #endregion

        public static PropertyInfo GetPrimaryKey<T>()
        {
            return GetPrimaryKey(typeof(T));
        }

        public static PropertyInfo GetPrimaryKey(Type type)
        {
            return type.GetProperties().Where(prop =>
            prop.IsPrimaryKey())
            .FirstOrDefault();
        }

        public static bool HasPrimaryKey<T>()
        {
            return HasPrimaryKey(typeof(T));
        }

        public static bool HasPrimaryKey(Type type)
        {
           return type.GetProperties().Where(prop =>
               prop.IsPrimaryKey())
               .Count()>0;

            // return GetPrimaryKey(type) != null;
        }
        #endregion
    }
}
