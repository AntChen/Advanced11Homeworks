﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.ModelMapping.Attributes
{

    public static class ValidateExtension
    {
        public static bool Validate<T>(T oObject)
        {
            Type type = oObject.GetType();
            foreach (var prop in type.GetProperties())
            {
                if (prop.IsDefined(typeof(AbstractValidateAttribute), true))
                {
                    object[] attributeArray = prop.GetCustomAttributes(typeof(AbstractValidateAttribute), true);
                    foreach (AbstractValidateAttribute attribute in attributeArray)
                    {
                        if (!attribute.Validate(prop.GetValue(oObject)))
                        {
                            return false;//表示终止
                        }
                    }
                }
            }
            return true;
        }
        public static bool Validate<T>(T oObject, out List<string> errorMsgs)
        {
            Type type = oObject.GetType();
            errorMsgs = new List<string>();
            foreach (var prop in type.GetProperties())
            {
                if (prop.IsDefined(typeof(AbstractValidateAttribute), true))
                {
                    object[] attributeArray = prop.GetCustomAttributes(typeof(AbstractValidateAttribute), true);
                    foreach (AbstractValidateAttribute attribute in attributeArray)
                    {
                        if (!attribute.Validate(prop.GetValue(oObject)))
                        {
                            //return false;//表示终止
                            errorMsgs.Add(attribute.ErrorMessage);
                        }
                    }
                }
            }
            return errorMsgs.Count == 0;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public abstract class AbstractValidateAttribute : Attribute
    {
        /// <summary>
        /// 验证失败的提示消息
        /// </summary>
        public string ErrorMessage { get; set; }
        public abstract bool Validate(object value);
    }

    /// <summary>
    /// 范围验证
    /// </summary>
    public class RangeAttribute : AbstractValidateAttribute
    {
        public double Min { get; set; }
        public double Max { get; set; }

        public RangeAttribute(double min, double max)
        {
            this.Max = max;
            this.Min = min;
        }

        public override bool Validate(object value)
        {
            return value != null && !string.IsNullOrWhiteSpace(value.ToString())
                && double.TryParse(value.ToString(), out double val)
                && val > this.Min && val < this.Max;
        }
    }
    /// <summary>
    /// 长度验证
    /// </summary>
    public class StringLengthAttribute : AbstractValidateAttribute
    {
        public long Min { get; set; }
        public long Max { get; set; }

        public StringLengthAttribute(long min, long max = long.MaxValue)
        {
            this.Max = max;
            this.Min = min;
        }

        public override bool Validate(object value)
        {
            //int length = value.ToString().Length;
            return value != null && !string.IsNullOrWhiteSpace(value.ToString())
                && value.ToString().Length >= this.Min && value.ToString().Length <= this.Max;
        }
    }
    /// <summary>
    /// 是否必须验证
    /// </summary>
    public class RequiredAttribute : AbstractValidateAttribute
    {
        public bool IsRequired { get; set; }

        public RequiredAttribute(bool isRequired = true)
        {
            this.IsRequired = isRequired;
        }
        public override bool Validate(object value)
        {
            return !IsRequired || value != null && !string.IsNullOrWhiteSpace(value.ToString());
        }
    }
    /// <summary>
    /// 正则验证
    /// </summary>
    public class RegexAttribute : AbstractValidateAttribute
    {
        public string RegexStr { get; set; }

        public RegexAttribute(string regexStr)
        {
            this.RegexStr = regexStr;
        }
        public override bool Validate(object value)
        {
            return value != null && !string.IsNullOrWhiteSpace(value.ToString()) && System.Text.RegularExpressions.Regex.IsMatch(value.ToString(), this.RegexStr);
        }
    }
    /// <summary>
    /// 邮箱验证
    /// </summary>
    public class EmailAttribute : RegexAttribute
    {
        public EmailAttribute()
            //: base(@"^[a-z]([a-z0-9]*[-_]?[a-z0-9]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+[\.][a-z]{2,3}([\.][a-z]{2})?$")
            : base(@"[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?")
        {

        }
    }
    /// <summary>
    /// 手机号码验证
    /// </summary>
    public class MobileAttribute : RegexAttribute
    {
        public MobileAttribute()
            : base(@"^((\(\d{2,3}\))|(\d{3}\-))?1{3,5-9}\d{9}$")
        {

        }
    }
}
