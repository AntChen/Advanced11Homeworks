﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.ModelMapping.Attributes
{
    /// <summary>
    /// 数据库实体映射 列特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        public ColumnAttribute(string columnName, bool isPrimaryKey = false, bool isAutoGrowth = false)
        {
            this.ColumnName = columnName;
            this.IsPrimaryKey = isPrimaryKey;
            this.IsAutoGrowth = isAutoGrowth;
        }
        /// <summary>
        /// 数据库表列名
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 是否是主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// 是否是自动增长
        /// </summary>
        public bool IsAutoGrowth { get; set; }


        public string GetDBColumnName()
        {
            return this.ColumnName;
        }
    }
}
