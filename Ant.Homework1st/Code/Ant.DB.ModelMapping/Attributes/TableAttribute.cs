﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.ModelMapping.Attributes
{
    /// <summary>
    /// 数据库实体映射 表特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        public TableAttribute(string tableName)
        {
            this.TableName = tableName;
        }
        /// <summary>
        /// 数据库表名
        /// </summary>
        public string TableName { get; set; }

        public string GetDBTableName()
        {
            return this.TableName;
        }
    }
}
