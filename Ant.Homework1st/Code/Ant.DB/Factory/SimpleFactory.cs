﻿using Ant.DB.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;

namespace Ant.DB.Factory
{
    /// <summary>
    /// 简单工厂
    /// </summary>
    public class SimpleFactory
    {
        static SimpleFactory()
        {
            string DBHelperName = ConfigurationManager.AppSettings["DBHelperName"].ToString();
            DBHelperTypeName = DBHelperName.Split(',')[0];
            DBHelperDLLName = DBHelperName.Split(',')[1];

            Assembly assembly = Assembly.Load(DBHelperDLLName);
            DBHelperType = assembly.GetType(DBHelperTypeName);
        }
        private static string DBHelperTypeName;
        private static string DBHelperDLLName;

        private static Type DBHelperType;

        /// <summary>
        /// 创建数据库操作帮助类
        /// </summary>
        /// <returns></returns>
        public static IDBHelper CreateHelper()
        {
            return (IDBHelper)Activator.CreateInstance(DBHelperType);
        }
    }
}
