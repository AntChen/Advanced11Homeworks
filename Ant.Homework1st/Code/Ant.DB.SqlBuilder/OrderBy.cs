﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.SqlBuilder
{
    /// <summary>
    ///排序
    /// </summary>
    public class OrderBy
    {
        /// <summary>
        /// 排序方式
        /// </summary>
        public OrderFlag OrderFlag { get; set; } = OrderFlag.Asc;
        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName { get; set; }

        public OrderBy() { }
        public OrderBy(string column, OrderFlag flag = OrderFlag.Asc)
        {
            this.ColumnName = column;
            this.OrderFlag = flag;
        }

        public override string ToString()
        {
            string order = SqlCodeAttribute.GetSqlCode(typeof(OrderFlag).GetField(this.OrderFlag.ToString()));
            return $"[{ColumnName}] {order}";
        }
    }
    /// <summary>
    /// 排序方式
    /// </summary>
    public enum OrderFlag
    {
        /// <summary>
        /// 升序
        /// </summary>
        [SqlCode("ASC")]
        Asc = 1,
        /// <summary>
        /// 降序
        /// </summary>
        [SqlCode("DESC")]
        Desc = 2
    }
}
