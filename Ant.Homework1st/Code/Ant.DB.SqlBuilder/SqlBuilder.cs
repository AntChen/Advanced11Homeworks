﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.SqlBuilder
{
    /// <summary>
    /// SQL 语句构建器
    /// </summary>
    public class SqlBuilder
    {
        #region 字段属性
        /// <summary>
        /// 字段与值 
        /// </summary>
        private IDictionary<string, string> _keyValues = new Dictionary<string, string>();

        private IDictionary<string, string> _whereKeyValues = new Dictionary<string, string>();

        /// <summary>
        /// 排序字段
        /// </summary>
        private List<OrderBy> _orders = new List<OrderBy>();

        /// <summary>
        /// 加载列
        /// </summary>
        private List<string> _columns = new List<string>();

        /// <summary>
        /// 忽略列
        /// </summary>
        private List<string> _ignoreColumns = new List<string>();

        private string _tableName = string.Empty;

        private string _top = string.Empty;

        private string _op = "SELECT";

        private Condition _condition = null;

        #endregion

        #region 构造方法

        /// <summary>
        /// SQL 语句构建器 构造方法
        /// </summary>
        public SqlBuilder() { }
        /// <summary>
        /// SQL 语句构建器 构造方法
        /// </summary>
        /// <param name="tableName">表名</param>
        public SqlBuilder(string tableName) { this._tableName = tableName; }

        #endregion

        /// <summary>
        /// 设置表名
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public SqlBuilder SetTable(string tableName)
        {
            this._tableName = tableName;
            return this;
        }

        #region Top

        public SqlBuilder SetTop(int top)
        {
            this._top = $"TOP ({top})";
            return this;
        }
        public Condition Where(Where where)
        {
            this._condition = where;
            return this._condition;
        }
        #endregion

        public SqlBuilder Select()
        {
            this._op = "SELECT";
            return this;
        }
        public SqlBuilder Select(params string[] columns)
        {
            _columns.AddRange(columns);
            return Select();
        }
        public SqlBuilder Update()
        {
            this._op = "UPDATE";
            return this;
        }

        public SqlBuilder Delete()
        {
            this._op = "DELETE";
            return this;
        }
        #region 排序方式

        /// <summary>
        /// 添加排序
        /// </summary>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public SqlBuilder AddOrderBy(OrderBy orderBy)
        {
            this._orders.Add(orderBy);
            return this;
        }
        /// <summary>
        /// 添加排序
        /// </summary>
        /// <param name="column">字段</param>
        /// <param name="flag">排序方式</param>
        /// <returns></returns>
        public SqlBuilder AddOrderBy(string column, OrderFlag flag = OrderFlag.Asc)
        {
            this._orders.Add(new OrderBy(column, flag));
            return this;
        }

        #endregion

        #region 构建

        public string ToSqlString()
        {
            StringBuilder stringBuilder = new StringBuilder(_op);

            if (_op.Equals("SELECT", StringComparison.CurrentCultureIgnoreCase))
            {
                stringBuilder.Append(_columns.Count == 0 ? " *" : string.Join(",", _columns));
            }
            else if (_op.Equals("UPDATE", StringComparison.CurrentCultureIgnoreCase))
            {

            }
            else if (_op.Equals("DELETE", StringComparison.CurrentCultureIgnoreCase))
            {

            }

            stringBuilder.Append($" FROM [{_tableName}]");

            #region Where 

            if (_condition != null)
            {
                var str = _condition.ToString();
                stringBuilder.AppendFormat(" WHERE {0}", str.Substring(1, str.Length - 2));
            }
            #endregion

            if (_op.Equals("SELECT", StringComparison.CurrentCultureIgnoreCase))
            {
                #region Order By
                if (_orders != null && _orders.Count > 0)
                {
                    stringBuilder.Append(" ORDER BY");
                    foreach (var order in _orders)
                    {
                        stringBuilder.AppendFormat(" {0}", order.ToString());
                    }

                }
                #endregion

                #region Group By

                #endregion
            }
            return stringBuilder.ToString();
        }

        #endregion

        public override string ToString()
        {
            return ToSqlString();
        }
    }
}
