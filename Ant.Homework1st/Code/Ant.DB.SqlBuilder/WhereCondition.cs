﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.SqlBuilder
{
    public class Condition
    {
        public string Column { get; set; }
        public object Value { get; set; }

        public virtual SqlCompareOperator CompareOperator { get; set; }
        protected virtual SqlLogicOperator LogicOperator { get; set; }

        protected List<Condition> subConditions;

        protected List<Condition> conditions;
        public Condition()
        {

        }
        public Condition(string column, object value, SqlCompareOperator compareOperator)
        {
            this.Column = column;
            this.Value = value;
            this.CompareOperator = compareOperator;
        }
        protected Condition(string column, object value, SqlCompareOperator compareOperator, SqlLogicOperator logicOperator)
        {
            this.Column = column;
            this.Value = value;
            this.CompareOperator = compareOperator;
            this.LogicOperator = logicOperator;
        }
        private void InitSubConditions()
        {
            if (subConditions == null) subConditions = new List<Condition>();
        }
        protected void InitConditions()
        {
            if (conditions == null) conditions = new List<Condition>();
        }

        public override string ToString()
        {
            string opera = SqlCodeAttribute.GetSqlCode(typeof(SqlCompareOperator).GetField(this.CompareOperator.ToString()));

            StringBuilder stringBuilder = new StringBuilder();
            string logicOpera = string.Empty;

            //if (!this.LogicOperator.Equals(SqlLogicOperator.None))
            //{
            //    logicOpera = SqlCodeAttribute.GetSqlCode(typeof(SqlLogicOperator).GetField(this.LogicOperator.ToString()));

            //    return $" {logicOpera} ({stringBuilder.ToString()})";

            //}
            //当前条件
            stringBuilder.Append($"[{Column}] {opera} {GetValueString()}");

            //平级条件
            if (conditions != null && conditions.Count > 0)
            {

                for (int i = 0; i < conditions.Count; i++)
                {
                    logicOpera = SqlCodeAttribute.GetSqlCode(typeof(SqlLogicOperator).GetField(conditions[i].LogicOperator.ToString()));
                    stringBuilder.AppendFormat(" {0} {1}", logicOpera, conditions[i].ToString());
                }
            }

            //子条件
            if (subConditions != null && subConditions.Count > 0)
            {
                for (int i = 0; i < subConditions.Count; i++)
                {
                    logicOpera = SqlCodeAttribute.GetSqlCode(typeof(SqlLogicOperator).GetField(subConditions[i].LogicOperator.ToString()));
                    stringBuilder.AppendFormat(" {0} ({1})", logicOpera, subConditions[i].ToString());
                }
            }


            return stringBuilder.ToString();
        }
        protected Condition AddSubCondition(Condition condition)
        {
            InitSubConditions();
            this.subConditions.Add(condition);
            return this;
        }
        public Condition And(Condition condition)
        {
            InitConditions();
            condition.LogicOperator = SqlLogicOperator.And;
            this.conditions.Add(condition);
            return this;
        }
        protected Condition Or(Condition condition)
        {
            InitConditions();
            condition.LogicOperator = SqlLogicOperator.Or;
            this.conditions.Add(condition);
            return this;
        }

        public Condition AddSubCondition(string column, object value, SqlCompareOperator compareOperator)
        {
            return AddSubCondition(new Condition(column, value, compareOperator, SqlLogicOperator.None));
        }
        public Condition And(string column, object value, SqlCompareOperator compareOperator)
        {
            return And(new Condition(column, value, compareOperator, SqlLogicOperator.And));
        }
        public Condition Or(string column, object value, SqlCompareOperator compareOperator)
        {
            return Or(new Condition(column, value, compareOperator, SqlLogicOperator.Or));
        }
        protected virtual string GetValueString()
        {
            if (!IsNumberType(Value.GetType()))
            {
                return $"'{this.Value.ToString()}'";
            }
            return this.Value.ToString();
        }
        protected static bool IsNumberType(Type type)
        {
            return type.Equals(typeof(int))
                || type.Equals(typeof(double))
                || type.Equals(typeof(decimal))
                || type.Equals(typeof(long));
        }
    }

    public class Where : Condition
    {
        public Where()
        {
            this.LogicOperator = SqlLogicOperator.None;
        }
        public Where(string column, object value, SqlCompareOperator compareOperator)
            : base(column, value, compareOperator, SqlLogicOperator.None)
        {
        }

        public override string ToString()
        {
            return $"({base.ToString()})";
        }
    }
    //public class And : Condition
    //{
    //    public And()
    //    {
    //        this.LogicOperator = SqlLogicOperator.And;
    //    }
    //    public And(string column, object value, SqlCompareOperator compareOperator)
    //        : base(column, value, compareOperator, SqlLogicOperator.And)
    //    {
    //    }
    //    public And Add(And and)
    //    {
    //        InitConditions();
    //        this.conditions.Add(and);
    //        return this;
    //    }
    //    public And Or(Or or)
    //    {
    //        InitConditions();
    //        this.conditions.Add(or);
    //        return this;
    //    }
    //}
    //public class Or : Condition
    //{
    //    public Or()
    //    {
    //        this.LogicOperator = SqlLogicOperator.Or;
    //    }
    //    public Or(string column, object value, SqlCompareOperator compareOperator)
    //        : base(column, value, compareOperator, SqlLogicOperator.Or)
    //    {
    //    }
    //    public void Add(And and)
    //    {
    //        InitConditions();
    //        this.conditions.Add(and);

    //    }
    //    public void Or(Or or)
    //    {
    //        InitConditions();
    //        this.conditions.Add(or);
    //    }
    //}
    //public class LikeCondition : WhereCondition
    //{
    //    public override SqlOperator Operator { get; set; } = SqlOperator.Like;

    //    public virtual string GetValueString()
    //    {
    //        if (!IsNumberType(Value.GetType()))
    //        {
    //            return $"'{this.Value.ToString()}'";
    //        }
    //        return this.Value.ToString();
    //    }
    //}

    /// <summary>
    /// SQL比较操作符
    /// </summary>
    public enum SqlCompareOperator
    {
        /// <summary>
        /// 等于
        /// </summary>
        [SqlCode("=")]
        Equal = 1,
        /// <summary>
        /// 不等于
        /// </summary>
        [SqlCode("<>")]
        Unequal = 2,
        /// <summary>
        /// 大于
        /// </summary>
        [SqlCode(">")]
        GreaterThan = 3,
        /// <summary>
        /// 小于
        /// </summary>
        [SqlCode("<")]
        LessThan = 4,

        [SqlCode("LIKE")]
        Like = 5,
    }

    public enum SqlLogicOperator
    {
        [SqlCode("")]
        None = 0,
        [SqlCode("AND")]
        And = 1,
        [SqlCode("OR")]
        Or = 2
    }
}
