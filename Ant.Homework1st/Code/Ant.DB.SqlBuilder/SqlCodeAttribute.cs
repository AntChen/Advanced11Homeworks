﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.SqlBuilder
{
    /// <summary>
    /// Sql代码特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field | AttributeTargets.Property)]
    public class SqlCodeAttribute : Attribute
    {
        /// <summary>
        /// Sql代码
        /// </summary>
        public string SqlCode { get; set; }

        public SqlCodeAttribute(string sqlCode)
        {
            this.SqlCode = sqlCode;
        }

        public static string GetSqlCode(PropertyInfo property)
        {
            SqlCodeAttribute sqlCodeAttribute = (SqlCodeAttribute)property.GetCustomAttribute(typeof(SqlCodeAttribute), true);
            if (sqlCodeAttribute == null) return property.Name;
            return sqlCodeAttribute.SqlCode;
        }
        public static string GetSqlCode(FieldInfo field)
        {
            SqlCodeAttribute sqlCodeAttribute = (SqlCodeAttribute)field.GetCustomAttribute(typeof(SqlCodeAttribute), true);
            if (sqlCodeAttribute == null) return field.Name;
            return sqlCodeAttribute.SqlCode;
        }
    }
}
