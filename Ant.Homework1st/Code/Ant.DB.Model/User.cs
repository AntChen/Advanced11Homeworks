﻿using Ant.DB.ModelMapping.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ant.DB.Model.Attributes;

namespace Ant.DB.Model
{
    /// <summary>
    /// 数据库--用户类
    /// </summary>
    [Table("User")]
    public partial class User : BaseModel
    {
        [Dispaly("姓名")]
        public string Name { get; set; }

        [StringLength(3, 12, ErrorMessage = "账户名长度只能在3-12位")]
        [Dispaly("账户名")]
        public string Account { get; set; }

        [Dispaly("用户密码", true)]
        public string Password { get; set; }
        
        [Email(ErrorMessage = "用户邮箱名错误")]
        [Dispaly("邮箱")]
        public string Email { get; set; }

        [Dispaly("手机号码")]
        public string Mobile { get; set; }

        [Dispaly("公司ID")]
        public int? CompanyId { get; set; }

        [Dispaly("公司名称")]
        public string CompanyName { get; set; }
        
        [Dispaly("用户状态")]
        [Column("State")]
        public AccountState Status { get; set; }

        [Dispaly("用户类型")]
        public AccountType UserType { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public DateTime CreateTime { get; set; }

        public int CreatorId { get; set; }

        public int? LastModifierId { get; set; }

        public DateTime? LastModifyTime { get; set; }
    }
}
