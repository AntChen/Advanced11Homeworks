﻿using Ant.DB.Model.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.Model
{
    /// <summary>
    /// 账户类型  1 普通用户 2 管理员 4 超级管理员
    /// </summary>
    public enum AccountType
    {
        /// <summary>
        /// 普通用户
        /// </summary>
        [Remark("普通用户")]
        User = 1,
        /// <summary>
        /// 管理员
        /// </summary>
        [Remark("管理员")]
        Admin = 2,
        /// <summary>
        /// 超级管理员
        /// </summary>
        [Remark("超级管理员")]
        SuperAdmin = 4
    }
}
