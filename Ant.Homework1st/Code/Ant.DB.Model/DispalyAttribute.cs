﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.Model.Attributes
{
    /// <summary>
    /// 显示名称属性
    /// </summary>
    public class DispalyAttribute : Attribute
    {
        public string Name { get; set; }
        public bool IsHide { get; set; }
        public DispalyAttribute(string name, bool isHide = false)
        {
            this.Name = name;
            this.IsHide = isHide;
        }
    }
}
