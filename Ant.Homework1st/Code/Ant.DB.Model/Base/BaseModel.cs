﻿using Ant.DB.ModelMapping.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.Model
{
    /// <summary>
    /// 数据基类
    /// </summary>
    public abstract class BaseModel
    {
        [Column("Id", true)]
        public virtual int Id { get; set; }
    }
}
