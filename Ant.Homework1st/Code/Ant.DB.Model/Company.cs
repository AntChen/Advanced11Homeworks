﻿using Ant.DB.Model.Attributes;
using Ant.DB.ModelMapping.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.Model
{
    /// <summary>
    /// 数据库--公司信息类
    /// </summary>
    [Table("Company")]
    public partial class Company : BaseModel
    {
        //public override int Id { get; set; }
        [Required(true, ErrorMessage = "公司名称不能为空")]
        [StringLength(3, ErrorMessage = "公司名称不能少于3个字符")]
        [Dispaly("公司名称")]
        public string Name { get; set; }

        [Dispaly("创建时间")]
        public DateTime CreateTime { get; set; }

        [Dispaly("创建者ID")]
        [Range(100, 200, ErrorMessage = "创建者ID需大于100 小于200")]
        public int CreatorId { get; set; }

        [Dispaly("修改者ID")]
        [Regex(@"^\d*$", ErrorMessage = "修改者ID需大于0")]
        public int? LastModifierId { get; set; }

        [Dispaly("上次修改时间")]
        public DateTime? LastModifyTime { get; set; }
    }
}
