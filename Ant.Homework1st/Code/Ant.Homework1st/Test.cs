﻿using Ant.DB.Interfaces;
using Ant.DB.Model;
using Ant.DB.ModelMapping.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Homework1st
{
    public class Test
    {
        IPrintToScreen print = new DispalyToScreen();

        public void Show()
        {
            TestFind();
            TestFindUser();

        }
        private void TestFind()
        {
            IDBHelper dBHelper = DB.Factory.SimpleFactory.CreateHelper();

            var companys = dBHelper.GetAll<Company>();
            print.Print(companys);

            var company = dBHelper.GetById<Company>(1);

            company.Name = "acv3";
            company.CreatorId = 189;
            print.Print(company);


        }
        private void TestFindUser()
        {
            IDBHelper dBHelper = DB.Factory.SimpleFactory.CreateHelper();

            var users = dBHelper.GetAll<User>();
            print.Print(users);

            var user = dBHelper.GetById<User>(1);
            user.LastModifyTime = DateTime.Now;
            
            print.Print(user);

            user.Email = "123@qq.com";
            Update(dBHelper, user);
        }

        private void Update<T>(IDBHelper dBHelper, T model) where T : BaseModel
        {
            bool res = dBHelper.Update<T>(model, out List<string> msg);
            foreach (var item in msg)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(res ? "更新成功" : "更新失败");
        }

    }
}
