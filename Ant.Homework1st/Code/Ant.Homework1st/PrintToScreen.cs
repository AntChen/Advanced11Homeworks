﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Ant.DB.Model;
using Ant.DB.Model.Attributes;

namespace Ant.Homework1st
{
    public interface IPrintToScreen
    {
        void Print<T>(params T[] models);
        void Print<T>(List<T> models);
    }
    public class PrintToScreen : IPrintToScreen
    {
        public void Print<T>(params T[] models)
        {
            Print(models.ToList());
        }
        public void Print<T>(List<T> models)
        {
            if (models != null && models.Count > 0)
            {
                var properties = typeof(T).GetProperties();
                Console.WriteLine($"-----------------------打印实体对象{typeof(T)} {models.Count} Start------------------------");
                foreach (var model in models)
                {
                    Console.WriteLine();
                    foreach (var prop in properties)
                    {
                        //if (IsHide(prop)) continue;
                        Console.WriteLine($"{prop.Name} : {prop.GetValue(model)}");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine($"-----------------------打印实体对象{typeof(T)} {models.Count} End------------------------");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("暂无实体对象数据");
                Console.WriteLine();
            }
        }
    }
    public class DispalyToScreen : IPrintToScreen
    {
        public void Print<T>(params T[] models)
        {
            Print(models.ToList());
        }
        public void Print<T>(List<T> models)
        {
            if (models != null && models.Count > 0)
            {
                var properties = typeof(T).GetProperties();
                Console.WriteLine($"-----------------------打印实体对象{typeof(T)} {models.Count} Start------------------------");
                DispalyAttribute attr;
                string sTemp = string.Empty;
                foreach (var model in models)
                {
                    Console.WriteLine();
                    foreach (var prop in properties)
                    {
                        //if (IsHide(prop)) continue;
                        if (prop.IsDefined(typeof(DispalyAttribute)))
                        {
                            attr = ((DispalyAttribute)prop.GetCustomAttribute(typeof(DispalyAttribute)));
                            if (attr.IsHide) continue;
                            // Console.WriteLine($"{attr.Name} : {prop.GetValue(model)}");
                            sTemp = attr.Name;
                        }
                        else
                        {
                            sTemp = prop.Name;
                            //Console.WriteLine($"{prop.Name} : {prop.GetValue(model)}");
                        }
                        if (prop.PropertyType.IsEnum)
                        {
                            Console.WriteLine($"{sTemp} : {((Enum)prop.GetValue(model)).GetRemark()}");
                        }
                        else
                        {
                            Console.WriteLine($"{sTemp} : {prop.GetValue(model)}");
                        }
                    }
                    Console.WriteLine();
                }
                Console.WriteLine($"-----------------------打印实体对象{typeof(T)} {models.Count} End------------------------");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("暂无实体对象数据");
                Console.WriteLine();
            }
        }

    }
}
