﻿本项目是Advanced11第一次homework

实现内容
1.数据库表映射基类BaseModel , 包含ID属性
	
2.数据库表名映射、字段映射 
	使用Ant.DB.ModelMapping.ColumnAttribute 可设置实体的数据库字段映射名
	使用Ant.DB.ModelMapping.TableAttribute	可设置实体的数据库表映射名
	
3.使用IDBHelper接口封装数据库访问操作
	使用简单工厂Ant.DB.Factory.SimpleFactory 读取配置创建相对应的数据库操作帮助类
	实现通过ID查询单个实体
	实现查出数据表中全部数据
	实现更新实体（自动通过Ant.DB.ModelMapping.ColumnAttribute 的设置是否为主键进行关联更新）
	实现删除实体（自动通过Ant.DB.ModelMapping.ColumnAttribute 的设置是否为主键进行关联删除）
	自动实现插入/更新数据验证（需要使用继承自Ant.DB.ModelMapping.Attributes.AbstractValidateAttribute的特性）
	使用方法Ant.DB.Interfaces.Insert<T>(T model, out List<string> validateErrorMsg)可获取到所有的验证错误信息
	
Update1
2018-4-29 18:40:24
 使用枚举来处理用户状态，用户账户类型
 主要更新文件：
	 添加文件：
		Ant.DB.Model/Attributes/RemarkAttribute.cs
	 修改文件：
		Ant.DB.Model/Enum/AccountState.cs
		Ant.DB.Model/Enum/AccountType.cs
		Ant.DB.Model/User.cs
		Ant.DB.SqlServer/SqlServerDBHelper.cs
		Ant.Homework1st/PrintToScreen.cs
		Ant.Homework1st/Test.cs