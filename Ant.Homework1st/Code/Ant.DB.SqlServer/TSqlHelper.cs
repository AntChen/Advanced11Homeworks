﻿using Ant.DB.ModelMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.SqlServer
{
    public class TSqlHelper<T>
    {
        static TSqlHelper()
        {
            Type type = typeof(T);
            PropertiesExceptPrimaryKey = type.GetProperties().Where(col => !col.IsPrimaryKey());
            PropertyPrimaryKey = MappingTool.GetPrimaryKey(type);
            TableName = MappingTool.GetTableName<T>();

            string columnString = string.Join(",", PropertiesExceptPrimaryKey.Select(col => string.Format("[{0}]", col.GetColumnName())));
            string columnInsertString = string.Join(",", PropertiesExceptPrimaryKey.Select(p => $"@{p.GetColumnName()}"));
            string columnUpdateString = string.Join(",", PropertiesExceptPrimaryKey.Select(p => $"{p.GetColumnName()} = @{p.GetColumnName()}"));

            FindByIdSql = $"SELECT {columnString} FROM [{TableName}] WHERE Id=";
            FindByPrimaryKeySql = $"SELECT {columnString} FROM [{TableName}] WHERE [{PropertyPrimaryKey.Name}] = @{PropertyPrimaryKey.Name};";
            FindAllSql = $"SELECT {columnString} FROM [{TableName}];";
            DeleteByIdSql = $"DELETE FROM [{TableName}] WHERE [{PropertyPrimaryKey.Name}] = @{PropertyPrimaryKey.Name};";

            UpdateAllFieldByIdSql = $"UPDATE FROM [{TableName}] SET {columnUpdateString} WHERE {PropertyPrimaryKey.Name} = @{PropertyPrimaryKey.Name}";
            InsertWithoutIdSql = $"INSERT INTO [{TableName}] ({columnString}) VALUES ({columnInsertString});";
        }
        public static string TableName = null;

        public static string FindByIdSql = null;
        public static string FindByPrimaryKeySql = null;
        public static string FindAllSql = null;
        public static string DeleteByIdSql = null;
        public static string UpdateAllFieldByIdSql = null;
        public static string InsertWithoutIdSql = null;

        public static IEnumerable<PropertyInfo> PropertiesExceptPrimaryKey = null;
        public static PropertyInfo PropertyPrimaryKey = null;

    }
}
