﻿using Ant.DB.Interfaces;
using Ant.DB.Model;
using Ant.DB.ModelMapping;
using Ant.DB.ModelMapping.Attributes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.SqlServer
{
    /// <summary>
    /// SqlServer数据库帮助类
    /// </summary>
    public class SqlServerDBHelper : IDBHelper
    {
        private static string ConnStr;
        static SqlServerDBHelper()
        {
            ConnStr = ConfigurationManager.ConnectionStrings["SqlServerConnStr"].ConnectionString;
        }

        public bool Delete<T>(T model) where T : BaseModel
        {
            Type type = typeof(T);

            if (!MappingTool.HasPrimaryKey(type))
            {
                throw new Exception($"class \"{type.FullName}\" not set PrimaryKey, please use ColumnAttribute for set. ");
            }

            var propPrimaryKay = TSqlHelper<T>.PropertyPrimaryKey;
            SqlParameter primaryKeySqlParameter = new SqlParameter($"@{propPrimaryKay.Name}", propPrimaryKay.GetValue(model));

            int affectRowCounts = 0;
            ExeSql(TSqlHelper<T>.DeleteByIdSql, cmd =>
            {
                affectRowCounts = cmd.ExecuteNonQuery();
            }, primaryKeySqlParameter);
            return affectRowCounts > 0;
        }

        public List<T> GetAll<T>() where T : BaseModel
        {
            List<T> result = new List<T>();
            ExeSql(TSqlHelper<T>.FindAllSql, cmd =>
            {
                var reader = cmd.ExecuteReader();
                result = reader.GetModelsFromDataReader<T>();
            });
            return result;
        }

        public T GetById<T>(int id) where T : BaseModel
        {
            T model = default(T);
            ExeSql($"{TSqlHelper<T>.FindByIdSql}{id};", (cmd) =>
            {
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    model = reader.GetModelFromDataReader<T>();
                }
            });
            return model;
        }

        public bool Insert<T>(T model) where T : BaseModel
        {
            return Insert(model, out List<string> msg);
        }

        public bool Insert<T>(T model, out List<string> validateErrorMsg) where T : BaseModel
        {
            if (!ValidateExtension.Validate(model, out validateErrorMsg))
            {
                return false;
            }

            var sqlParameters = TSqlHelper<T>.PropertiesExceptPrimaryKey.Select(col => new SqlParameter($"@{col.GetColumnName()}", col.GetValue(model) ?? DBNull.Value)).ToList();

            int affectRowCounts = 0;
            ExeSql(TSqlHelper<T>.InsertWithoutIdSql, (cmd) =>
            {
                affectRowCounts = cmd.ExecuteNonQuery();
            }, sqlParameters.ToArray());

            return affectRowCounts > 0;
        }

        public bool Update<T>(T model) where T : BaseModel
        {
            return Update(model, out List<string> msg);
        }

        public bool Update<T>(T model, out List<string> validateErrorMsg) where T : BaseModel
        {
            if (!ValidateExtension.Validate(model, out validateErrorMsg))
            {
                return false;
            }
            Type type = typeof(T);
            if (!MappingTool.HasPrimaryKey(type))
            {
                throw new Exception($"class \"{type.FullName}\" not set PrimaryKey, please use ColumnAttribute for set. ");
            }
            var sqlParameters = TSqlHelper<T>.PropertiesExceptPrimaryKey.Select(col => new SqlParameter($"@{col.GetColumnName()}", col.GetValue(model) ?? DBNull.Value)).ToList();

            sqlParameters.Add(new SqlParameter($"@{TSqlHelper<T>.PropertyPrimaryKey.GetColumnName()}", TSqlHelper<T>.PropertyPrimaryKey.GetValue(model)));

            int affectRowCounts = 0;
            ExeSql(TSqlHelper<T>.UpdateAllFieldByIdSql, (cmd) =>
            {
                affectRowCounts = cmd.ExecuteNonQuery();
            }, sqlParameters.ToArray());
            return affectRowCounts > 0;
        }


        private void ExeSql(string sqlStr, Action<SqlCommand> action, params SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                var cmd = new SqlCommand(sqlStr, conn);
                cmd.Parameters.AddRange(parameters);
                action(cmd);
                //var affectRowCounts = cmd.ExecuteNonQuery();
                //return affectRowCounts > 0;
            }
        }
    }
}
