﻿using Ant.DB.ModelMapping.Attributes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.DB.SqlServer
{
    public static class SqlServerExtendsion
    {
        /// <summary>
        /// 从DataReader中获取数据实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataReader"></param>
        /// <returns></returns>
        public static T GetModelFromDataReader<T>(this SqlDataReader dataReader)
        {
            if (dataReader == null || !dataReader.HasRows) return default(T);
            var colTemp = string.Empty;
            Type type = typeof(T);
            T model = (T)Activator.CreateInstance(type);

            //DataColumnCollection tableColumns = dataReader.GetSchemaTable().Columns;

            foreach (var prop in type.GetProperties())
            {
                //获取到实体的列名
                colTemp = prop.IsDefined(typeof(ColumnAttribute), true) ? ((ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true)[0]).GetDBColumnName() : prop.Name;
                //判断数据表中是否存在与colTemp一样的字段
                //if (tableColumns.Contains(colTemp))
                //{
                prop.SetValue(model, dataReader[colTemp] == DBNull.Value ? null : dataReader[colTemp]);
                //}
            }
            return model;
        }
        /// <summary>
        /// 从DataReader中获取数据实体集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataReader"></param>
        /// <returns></returns>
        public static List<T> GetModelsFromDataReader<T>(this SqlDataReader dataReader)
        {
            if (dataReader == null || !dataReader.HasRows) return default(List<T>);
            var colTemp = string.Empty;
            Type type = typeof(T);

            T model = default(T);

            List<T> result = new List<T>();

            //DataColumnCollection tableColumns = dataReader.GetSchemaTable().Columns;
            while (dataReader.Read())
            {
                model = (T)Activator.CreateInstance(type);
                foreach (var prop in type.GetProperties())
                {
                    //获取到实体的列名
                    colTemp = prop.IsDefined(typeof(ColumnAttribute), true) ? ((ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true)[0]).GetDBColumnName() : prop.Name;
                    //判断数据表中是否存在与colTemp一样的字段
                    //if (tableColumns.Contains(colTemp))
                    //{
                    prop.SetValue(model, dataReader[colTemp] == DBNull.Value ? null : dataReader[colTemp]);
                    //}
                }
                result.Add(model);
            }

            return result;
        }
    }
}
